<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 1.10.2015
 * Time: 11:39
 */

$initec = '{"repository": {"website": "http://initec.safertechnology.eu/", "fork": false, "name": "initec", "scm": "git", "owner": "eset_developer", "absolute_url": "/eset_developer/initec/", "slug": "initec", "is_private": true}, "truncated": false, "commits": [{"node": "a7fe41564307", "files": [{"type": "modified", "file": "updates/.gitignore"}], "raw_author": "Erik Ivanov ", "utctimestamp": "2015-10-01 08:25:33+00:00", "author": "MasterErik", "timestamp": "2015-10-01 10:25:33", "raw_node": "a7fe415643074ea0dc9d6c57da9b61342a5b4a67", "parents": ["43138466ff02"], "branch": "master", "message": "test\n", "revision": null, "size": -1}], "canon_url": "https://bitbucket.org", "user": "MasterErik"}';

$deploy = '{"repository": {"website": "http://initec.safertechnology.eu/", "fork": false, "name": "initec", "scm": "git", "owner": "eset_developer", "absolute_url": "/eset_developer/initec/", "slug": "initec", "is_private": true}, "truncated": false, "commits": [{"node": "47779745ae26", "files": [{"type": "modified", "file": "updates/.gitignore"}], "raw_author": "Erik Ivanov ", "utctimestamp": "2015-10-01 08:37:37+00:00", "author": "MasterErik", "timestamp": "2015-10-01 10:37:37", "raw_node": "47779745ae26270cafaf9c2197b6e114e52cbfed", "parents": ["a7fe41564307"], "branch": "master", "message": "test\n", "revision": null, "size": -1}], "canon_url": "https://bitbucket.org", "user": "MasterErik"}';
$dotplant2 = '{"repository": {"website": "http://safertechnology.eu/", "fork": false, "name": "dotplant2", "scm": "git", "owner": "MasterErik", "absolute_url": "/MasterErik/dotplant2/", "slug": "dotplant2", "is_private": true}, "truncated": false, "commits": [{"node": "f759b64d26d7", "files": [{"type": "modified", "file": "application/migrations/m150317_070330_backgroundtasks_task_add_options.php"}], "raw_author": "Erik Ivanov ", "utctimestamp": "2015-10-01 11:04:10+00:00", "author": "MasterErik", "timestamp": "2015-10-01 13:04:10", "raw_node": "1874561a1633fff6d1535f1ff925e7a77f74ffe1", "parents": ["f759b64d26d7", "9023f4e427cb"], "branch": "release", "message": "Merge branch \'test_beta_2.0\' into release\n", "revision": null, "size": -1}], "canon_url": "https://bitbucket.org", "user": "MasterErik"}';

$_POST['payload'] = $dotplant2;

require_once 'bitbucket.php';

