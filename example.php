<?php

  require(__DIR__ . "/vendor/autoload.php");

  class MyWebhook extends Webhooks\Webhook {
    function isPayloadModify(&$options) {
      $options["#isPayload"] = false;
    }
    function onPushModify(&$options) {
      $options["#isPush"] = false;
      $this->_setLogEntry("Set the #isPush checks to false.");
      $this->_invoke("onPushAlter", $options);
    }
  }

  class MyNewWebhook extends MyWebhook {
    function onPushAlter(&$options) {
      $options["#isPush"] = true;
      $this->_setLogEntry("Set the #isPush checks to true.");
    }
	
	function _saveLog( $_ ) {

		  if( $_ ) {
			// Save the value
			file_put_contents(
			  'deployments.log',
			  $this->getLog(true),
			  FILE_APPEND
			  );
		  }
		
	}
  }

  // Instantiate the class
  //$webhook = new Webhooks\Webhook();
  $webhook = new MyNewWebhook();

  // Set the provider
  $webhook->setProvider("Bitbucket");

  // Set the remote repository address
  $webhook->setRemote("git@bitbucket.org:wppw/wppw-git-deploy.git");

  // Set the remote repository secret
  $webhook->setSecret("");

  // Add all the alias
  $webhook->setAlias("branch", "/var/www/ah0/data/www/eset.ah0.ru/wppw-git-deploy/");

  // Set to save the log into a file
  $webhook->saveLog(true);
  $webhook->_saveLog(true);

  // Check for the payload data
  if($webhook->getPayload()) {
    if($webhook->onPush()) {
      $webhook->gitPull("master");
    }
  }

?>

