<?php
/**
 * The repos that we want to deploy.
 *
 * Each repos will be an entry in the array in the following way:
 * 'repo name' => array( // Required. This is the repo name
 *        'path'   => '/path/to/local/repo/', // Required. The local path to your code.
 *        'branch' => 'the_desired_deploy_branch', // Required. Deployment branch.
 *        'remote' => 'git_remote_repo', // Optional. Defaults to 'origin'
 *        'post_deploy' => 'callback' // Optional callback function for whatever.
 *        'secret' => '' // Optional. The secret specified in the webhook settings (Only works with GitHub).
 * )
 *
 * You can put as many of these together as you want, each one is simply
 * another entry in the $repos array. To set up a deploy create a deploy key
 * for your repo on github or bitbucket. You can generate multiple deploy keys
 * for multiple repos.
 * @see https://confluence.atlassian.com/pages/viewpage.action?pageId=271943168
 *
 * Note that deploy keys are only necessary if the repo is private. If it is a
 * public repo, then you do not need a key to get read only access to the repo
 * which is really what we are after for deployment.
 *
 * Once you have done an initial git pull in the desired code location, you can
 * run 'pwd' to get the full directory of your git repo. Once done, enter that
 * full path in the 'path' option for that repo. The optional callback will allow
 * you to ping something else as well such as hitting a DB update script or any
 * other configuration you may need to do for the newly deployed code.
 */
$repos = [
    'initec' => [
        'branch' => 'master',
        'remote' => 'origin',
        'path' => '/var/www/developer/data/www/nod32.fi/',
        'secret' => ''
    ],
    'initec' => [
        'branch' => 'master',
        'remote' => 'origin',
        'path' => '/var/www/developer/data/www/initec.safertechnology.eu/',
        'secret' => ''
    ],
    'dotplant2' => [
        'branch' => 'release',
        'remote' => 'origin',
        'path' => '/var/www/developer/data/www/safertechnology.eu/',
        'secret' => ''
    ],
    'git-deploy' => [
        'branch' => 'master',
        'remote' => 'origin',
        'path' => '/var/www/developer/data/www/git-deploy/',
        'secret' => ''
    ],
    'wppw-git-deploy' => [
        'branch' => 'master',
        'remote' => 'origin',
        'path' => '/var/www/ah0/data/www/eset.ah0.ru/wppw-git-deploy/',
        'secret' => ''
    ],	

];

/**
 * Sets the deploy log directory
 */
define('DEPLOY_LOG_DIR', dirname(__FILE__));

/* Do not edit below this line */
require_once 'inc/class.deploy.php';
