<?php

// Make sure we have a payload, stop if we do not.
if (!isset($_POST['payload']))
	die('<h1>No payload present</h1><p>A BitBucket POST payload is required to deploy from this script.</p>');

/**
 * Tell the script this is an active end point.
 */
define('ACTIVE_DEPLOY_ENDPOINT', true);

require_once 'deploy-config.php';

// Logger 
require_once 'logger.php';

/**
 * Deploys BitBucket git repos
 */
class BitBucket_Deploy extends Deploy {

	/**
	 * Decodes and validates the data from bitbucket and calls the
	 * deploy constructor to deploy the new code.
	 *
	 * @param    string $payload The JSON encoded payload data.
	 */
	function __construct($payload, $headers) {

		function stripslashes_deep($value) {
			$value = is_array($value) ?
					array_map('stripslashes_deep', $value) :
					stripslashes($value);

			return $value;
		}

		$payload = json_decode(stripslashes_deep($payload), TRUE);
		//$payload_log = is_array($payload) ? print_r($payload, TRUE) : $payload;
		//$this->log('$payload_log: ' . $payload_log); //1 Log
		$name = $payload['repository']['name'];
		//$this->log('name:' . $name . ' branch:' . $payload['commits'][0]['branch']);
		
		// if branch empty
		if ( empty( $payload['commits'][0]['branch']) ) {
			// Logger 
			Logger::$PATH = dirname(__FILE__);
			Logger::getLogger('deployments')->log($payload);
		}
		
		if (isset(parent::$repos[$name]) && parent::$repos[$name]['branch'] === $payload['commits'][0]['branch']) {
			$data = parent::$repos[$name];
			$data['commit'] = $payload['commits'][0]['node'];
			parent::__construct($name, $data, $payload, $headers);
		}
	}

}

//https://bitbucket.org/site/master/issues/11537/webhooks-payload-empty
$_payload = !empty($_POST['payload']) ? $_POST['payload'] : file_get_contents('php://input');

// Start the deploy attempt.
new BitBucket_Deploy($_payload, getallheaders());
