<?php

  namespace Webhooks;

  /**
   * Class with basic properties and methods. Basically, its serve with
   * generic methods to load and verify the folder, the request data and the
   * command functions.
   *
   * @package    PHP
   * @author     Eduardo Segura <zero.duh@gmail.com>
   * @copyright  2016 ZEPLOY.com
   * @link       http://www.zeploy.com
   */
  abstract class WebhookAbstract implements WebhookInterface {

    /**
     * Padronize the code errors
     * @var        int
     */
    const LOG = 100;
    const SUCCESS = 200;
    const WARNING = 300;
    const ERROR = 400;

    /**
     * The payload data from the repository
     * @var        mix
     */
    static public $_payload;

    /**
     * The headers of the request
     * @var        array
     */
    static public $_headers;

    /**
     * The repository remote name or address
     * @var        string
     */
    protected $_repository;

    /**
     * The aliases folders and names
     * @var        array
     */
    protected $_aliases;

    /**
     * Save for the log
     * @var        boolean
     */
    protected $_save;

    /**
     * The log data
     * @var        array
     */
    protected $_log;

    /**
     * The last return from a execute command
     * @var        mix
     */
    protected $_lastCommandOutput;

    /**
     * The name of the remote repository
     * @var        string
     */
    protected $_providerName;

    /**
     * The loaded provider
     * @var        string
     */
    protected $_provider;

    /**
     * The secret of the provider
     * @var        string
     */
    protected $_secret;

    /**
     * Server path
     * @var        string
     */
    private $__rootPath;

    /**
     * Server protocol
     * @var        string
     */
    private $__serverProtocol;

    /**
     * Server remote address
     * @var        string
     */
    private $__remoteAddress;

    /**
     * Load and sets the basic data
     */
    public function __construct($provider = "", $repository = "", $secret = "") {
      $this->setProvider($provider);
      $this->setRepository($repository);
      $this->setSecret($secret);
      $this->_setHeaders();
      $this->__setServerProtocol();
      $this->__setRemoteAddress();
      $this->__setRootPath();
      $this->_setLogEntry("Command user: " . implode(",", $this->_exec("whoami", "2>&1")));
    }

    /**
     * Perform additional actions
     */
    public function __destruct() {
      $this->_setLogEntry("Finished.");
      $this->_saveLog();
    }

    /**
     * Sets the server protocol provided by the php or manually sets the
     * default.
     */
    private function __setServerProtocol() {
      $this->__serverProtocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
      $this->_setLogEntry("Server Protocol: " . $this->__serverProtocol);
    }

    /**
     * Sets the remote address that sent the request
     */
    private function __setRemoteAddress() {
      $this->__remoteAddress = $_SERVER['REMOTE_ADDR'];
      $this->_setLogEntry("Remote Address: " . $this->__remoteAddress);
    }

    /**
     * Based on the current folder, sets the root path
     */
    private function __setRootPath() {
      $this->__rootPath =  dirname(__DIR__) . '/';
      $this->_setLogEntry("Root Path: " . $this->__rootPath);
    }

    /**
     * Add the headers of the requisition for aditional checks
     */
    protected function _setHeaders() {
      foreach($_SERVER as $name => $value) {
        if(substr($name,0,5) == "HTTP_") {
          $name = substr($name,5);
        }

        $name = str_replace(" ","-",ucwords(strtolower(str_replace("_"," ",$name))));
        self::$_headers[$name] = $value;
      }
    }

    /**
     * Saves the log in a simple txt file
     */
    protected function _saveLog() {
      if($this->_save) {
        // Save the value
        file_put_contents(
          $_SERVER['SCRIPT_FILENAME'].'.log',
          $this->getLog(true),
          FILE_APPEND
          );
      }
    }

    /**
     * Adds a new message (or an array of messages) to the log. Additonally, its
     * converts a list of objects to array.
     *
     * @param      mix   $entry  The message(s)
     * @param      int   $type   The code of the message
     */
    protected function _setLogEntry($entry, $type = self::LOG) {
      if(!is_array($entry) && !is_string($entry) && is_object($entry)) {
        $entry = var_export($entry);
      }

      if(is_array($entry)) {
        foreach ($entry as $key => $value) {
          $this->_setLogEntry($value, $type);
        }
      } else if(!empty($entry)) {
        $this->_log[] = [
            "type" => $type,
            "message" => $entry,
          ];
      }
    }

    /**
     * Formats all the log array to a string.
     *
     * @param      array   $data   An array with the code and the message text
     *
     * @return     string
     */
    protected function _logToString($data) {
      $output = "";
      foreach ($data as $key => $log) {
        $output .= "[".strtoupper($log["type"])."]"." ".$log["message"]."\n";
      }
      return $output;
    }

    /**
     * Checks if the remote address is not empty, logging the checks.
     *
     * @return     boolean
     */
    protected function _isValidRemoteAddress() {
      if(empty($this->__remoteAddress)) {
        $this->_setLogEntry("The remote address is not valid!", self::ERROR);
        return false;
      }
      return true;
    }

    /**
     * Checks if the alias path exists and is a folder, logging the checks.
     *
     * @param      string   $name   The name of the alias
     *
     * @return     boolean
     */
    protected function _isValidFolder($name) {
      if(isset($this->_aliases[$name])) {
        if(is_dir($this->__rootPath.$this->_aliases[$name])) {

          return true;

        } else {
          $this->_setLogEntry("Invalid folder: \"".$this->__rootPath.$this->_aliases[$name]."\".", self::ERROR);
        }
      } else {
        $this->_setLogEntry("The aliase: ".$name." was not set or don't exists.", self::ERROR);
      }
      return false;
    }

    /**
     * Executes a command in the server console.
     *
     * @param      string  $cmd    The command value
     * @param      string  $op     Aditional operation
     *
     * @return     mix     NULL if the command cannot was executed, otherwise TRUE.
     */
    protected function _exec($cmd, $op) {
      $output = "";

      if (substr(php_uname(), 0, 7) == "Windows"){
        pclose(popen("start /B ". $cmd, "r"));
      } else {
        exec($cmd . " " . $op, $output);
      }

      if(empty($output)) {
        return NULL;
      }
      return $output;
    }

    /**
     * Acess the alias path and executes a command
     *
     * @param      string   $name   The name of the alias
     * @param      string   $cmd    The command to be executed
     *
     * @return     boolean
     */
    protected function _execute($name, $cmd, $operator) {
      $originalPath = getcwd();
      if(chdir($this->__rootPath.$this->_aliases[$name])) {
        $this->_setLogEntry("Acessed path: ".$this->__rootPath.$this->_aliases[$name], self::SUCCESS);
        $this->_setLogEntry("Command exec: ".$cmd, self::SUCCESS);

        $output = $this->_exec($cmd, $operator);
        if(is_null($output)) {
          $this->_setLogEntry("NULL", self::ERROR);
        } else {
          $this->_setLogEntry($output);
        }
        $this->_lastCommandOutput = $output;

        chdir($originalPath);
        return true;

      }

      $this->_setLogEntry("The path don't exists: ".$this->__rootPath.$this->_aliases[$name], self::ERROR);
      return false;
    }

    /**
     * Sets and check the payload data
     *
     * @param      object  $payload  The payload object
     *
     * @return     string
     */
    protected function _setPayload($payload) {
      self::$_payload = $payload;

      if($this->_isValidRemoteAddress()) {
        if($this->isPayload()) {
          return true;
        }
      }
      header($this->__serverProtocol.' 400 Bad Request');
      return false;
    }

    /**
     * Gets the a json request
     *
     * @return     mix
     */
    protected function _getJson($key) {
      return json_decode(file_get_contents('php://input'));
    }

    /**
     * Gets a post or get resquest
     *
     * @return     mix
     */
    protected function _getPost($key) {
      return json_decode(filter_input(INPUT_POST, $key));
    }

    /**
     * Let a extended class change the result of a function
     *
     * @param      string  $function  The name of the function
     * @param      string  $params    (description)
     *
     * @return     mix
     */
    protected function _invoke($function, &$params = []) {
      if(method_exists(get_class($this), $function)) {
        $this->{$function}($params);
      }
    }


    /**
     * Sets the name of the remote repository. Defaults sets the class name.
     *
     * @param      string  $name   Set a name for the remote name
     */
    public function setProvider($name) {
      if(!empty($name)) {
        $this->_providerName = $name;
        $provider = "\\Webhooks\\Providers\\".$this->_providerName;
        $this->_provider = new $provider();
      }
    }

    /**
     * Sets the repository name or the address.
     *
     * @param      string  $remote  The remote name or address
     */
    public function setRepository($repository) {
      $this->_repository = $repository;
    }

    /**
     * Sets the repository secret.
     *
     * @param      string  $secret  The secret string
     */
    public function setSecret($secret) {
      $this->_secret = $secret;
    }

    /**
     * Return the remote value
     *
     * @return     string
     */
    public function getRemote() {
      return $this->_repository;
    }

    /**
     * Adds and alias name with the folder path
     *
     * @param      string  $name   The alias name
     * @param      string  $url    The path to the folder
     */
    public function setAlias($name, $url) {
      $this->_aliases[$name] = $url;
    }

    /**
     * Returns the root path value
     *
     * @return     string
     */
    public function getRootPath() {
      return $this->__rootPath;
    }

    /**
     * Sets the save boolean
     *
     * @param      boolean  $bool   The save status
     */
    public function saveLog($bool) {
      $this->_save = $bool;
    }

    /**
     * Return the formatted log messages
     *
     * @param      boolean  $timeStamp  Add a timestamp
     * @param      boolean  $format     Change the return type
     *
     * @return     mix
     */
    public function getLog($timeStamp = false, $format = true) {
      $logData = $this->_log;

      if($timeStamp) {
        // Set the date
        array_unshift($logData, [
            "type" => self::LOG,
            "message" => "Date: ".date("F j, Y, g:i a"),
          ]);
      }

      if($format) {
        return $this->_logToString($logData)."\n";
      }
      return $logData;
    }

    /**
     * Executes a command in a valid folder
     *
     * @param      string   $name   The alias name
     * @param      string   $cmd    The command
     * @param      string   $op     Set aditional commands
     *
     * @return     boolean
     */
    public function exec($name, $cmd, $op = "2>&1") {
      if($this->_isValidFolder($name)) {
        return $this->_execute($name, $cmd, $op);
      }
      return false;
    }

    /**
     * Return the output of the last console command executed
     *
     * @return     string
     */
    public function getLastCommandOutput() {
      return $this->_lastCommandOutput;
    }

    /**
     * Gets the payload from a type of request
     *
     * @param      string   $type   The type of the payload
     * @param      string   $key    If a post the name of the key
     *
     * @return     boolean
     */
    public function getPayload($type = "", $key = "payload") {
      if( empty($type) &&
          isset(self::$_headers["Content-Type"])
        ) {

        switch (self::$_headers["Content-Type"]) {
          case "application/json":
            $type = "json";
            break;
          case "application/x-www-form-urlencoded":
            $type = "post";
            break;
          default:
            break;
        }

      }

      if(method_exists($this, "_get".ucfirst($type))) {
        return $this->_setPayload($this->{"_get".ucfirst($type)}($key));
      }

      return false;
    }
  }

?>
