<?php

  namespace Webhooks\Providers;
  use \Webhooks as WH;

  /**
   * Manage the payload data from the bitbucket repositories.
   *
   * @package    PHP
   * @author     Eduardo Segura <zero.duh@gmail.com>
   * @copyright  2016 ZEPLOY.com
   * @link       http://www.zeploy.com
   */
  class Bitbucket implements WH\WebhookInterface {

    /**
     * Check if the payload request is valid
     *
     * @param      array  $options  Filter options
     *
     * @return     boolean
     */
    public function isPayload(&$options) {
     if( !empty(WH\Webhook::$_payload) &&
        isset(WH\Webhook::$_headers["User-Agent"]) &&
        strpos(WH\Webhook::$_headers["User-Agent"], "Bitbucket-Webhooks") !== false
      ) {
        $options["#isPayload"] = true;
      }
    }

    /**
     * Check if is a push event
     *
     * @param      array  $options  Filter options
     *
     * @return     boolean
     */
    public function onPush(&$options) {
      if( isset(WH\Webhook::$_headers["X-Event-Key"]) &&
          WH\Webhook::$_headers["X-Event-Key"] == "repo:push"
        ) {

        $options["#isPush"] = true;
        $changes = current(WH\Webhook::$_payload->push->changes);

        foreach ($options as $option => $values) {
          if(in_array($option, ["new", "old"])) {
            foreach ($values as $key => $value) {
              if(in_array($key, ["name", "type"])) {
                if($value != $changes->{$option}->{$key}) {
                  $options["#isPush"] = false;
                }
              }
            }
          }
        }
      }
    }

    public function onFork(&$options) {}
  }

?>
