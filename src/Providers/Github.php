<?php

  namespace Webhooks\Providers;
  use \Webhooks as WH;

  /**
   * Manage the payload data from the github repositories.
   *
   * @package    PHP
   * @author     Eduardo Segura <zero.duh@gmail.com>
   * @copyright  2016 ZEPLOY.com
   * @link       http://www.zeploy.com
   */
  class Github implements WH\WebhookInterface {

    /**
     * Check if the payload request is valid
     *
     * @param      array  $options  Filter options
     *
     * @return     boolean
     */
    public function isPayload(&$options) {
      if( !empty(WH\Webhook::$_payload) &&
          isset(WH\Webhook::$_headers["User-Agent"]) &&
          strpos(WH\Webhook::$_headers["User-Agent"], "GitHub-Hookshot") !== false
        ) {
        $options["#isPayload"] = true;
      }
    }

    /**
     * Check if is a push event
     *
     * @param      array  $options  Filter options
     *
     * @return     boolean
     */
    public function onPush(&$options) {
      if( isset(WH\Webhook::$_headers["X-Github-Event"]) &&
          WH\Webhook::$_headers["X-Github-Event"] == "push"
        ) {

        $options["#isPush"] = true;
        $changes = WH\Webhook::$_payload;

        foreach ($options as $option => $values) {
          if(in_array($option, ["new", "old"])) {
            foreach ($values as $key => $value) {
              if( $key == "name" &&
                  "refs/heads/".$value != $changes->ref
                ) {
                $options["#isPush"] = false;
              }
            }
          }
        }
      }
    }

    /**
     * Check if is a fork event
     *
     * @param      array  $options  Filter options
     *
     * @return     boolean
     */
    public function onFork(&$options) {}
  }

?>
