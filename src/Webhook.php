<?php

  namespace Webhooks;

  /**
   * Class with basic properties and methods. Basically, its serve with
   * generic methods to load and verify the folder, the request data and the
   * command functions.
   *
   * @package    PHP
   * @author     Eduardo Segura <zero.duh@gmail.com>
   * @copyright  2016 ZEPLOY.com
   * @link       http://zeploy.com
   */
  class Webhook extends WebhookAbstract {

    /**
     * Basic check if the payload is valid
     *
     * @param      &array     $options  Array with options
     *
     * @return     boolean
     */
    public function isPayload(&$options = []) {
      // Process the options
      $options["#isPayload"] = false;

      // Call the provider checks
      $this->_provider->isPayload($options);

      // Give a chance to alter this function
      $this->_invoke("isPayloadModify", $options);

      if($options["#isPayload"]) {
        $this->_setLogEntry("The ".$this->_providerName." payload is valid.", self::SUCCESS);
      } else {
        $this->_setLogEntry("The ".$this->_providerName." payload is empty or is invalid.", self::WARNING);
      }
      return $options["#isPayload"];
    }

    /**
     * Basic check for a push payload
     *
     * @param      &array     $options  Array with options
     *
     * @return     boolean
     */
    public function onPush(&$options = []) {
      // Process the options
      if(isset($options["any"])) {
        $options["new"] = $options["any"];
        $options["old"] = $options["any"];
      }
      $options["#isPush"] = false;

      // Call the provider checks
      $this->_provider->onPush($options);

      // Give a chance to alter this function
      $this->_invoke("onPushModify", $options);

      if($options["#isPush"]) {
        $this->_setLogEntry("Received a push event.", self::SUCCESS);
      }
      return $options["#isPush"];
    }

    public function onFork(&$options = []) {

    }

    /**
     * Check if the current folder is a git one.
     *
     * @param      string  $name   Alias name
     *
     * @return     bool
     */
    public function gitVerify($name) {
      $options = [
        "#name" => $name,
        "#command" => "git rev-parse --verify --quiet ".$name,
        "#operation" => "2>/dev/null>&1"
      ];

      // Give a chance to alter this function
      $this->_invoke("gitVerifyModify", $options);

      $this->exec($name, $options["#command"], $options["#operation"]);
      return is_null($this->_lastCommandOutput) ? false : true;
    }

    /**
     * Clone the repository branch. command.
     *
     * @param      string   $name   Alias name
     *
     * @return     boolean
     */
    public function gitClone($name) {
      $options = [
        "#name" => $name,
        "#command" => "git clone -b ".$name." ".$this->_repository." .",
        "#operation" => "2>&1"
      ];

      // Give a chance to alter this function
      $this->_invoke("gitCloneModify", $options);

      return $this->exec($name, $options["#command"], $options["#operation"]);
    }

    /**
     * Pull the new contents from the repository branch
     *
     * @param      string   $name   Alias name
     *
     * @return     boolean
     */
    public function gitPull($name) {
      $options = [
        "#name" => $name,
        "#command" => "git pull ".$this->_repository." ".$name,
        "#operation" => "2>&1"
      ];

      // Give a chance to alter this function
      $this->_invoke("gitPullModify", $options);

      return $this->exec($name, $options["#command"], $options["#operation"]);
    }
  }

?>
