<?php

  namespace Webhooks;

  interface WebhookInterface {
    /**
     * Checks for a valid Payload
     *
     * @param      mix     $options
     */
    public function isPayload(&$options);

    /**
     * The push method to be checked
     *
     * @param      string  $options
     */
    public function onPush(&$options);

    /**
     * The fork method to be checked
     *
     * @param      string  $options
     */
    public function onFork(&$options);
  }

?>
